// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
let data = {
    projects: [
        { key: "ForbesPE", value: "ForbesPE" },
        { key: "Advisor", value: "Advisor" },
        { key: "MSN", value: "MSN" },
        { key: "CM", value: "CM" },
        { key: "Generic", value: "Generic" }
    ]
};

document.querySelector("a").addEventListener("click", getData);

function getData() {
    //   document.getElementById("demo").innerHTML = "Hello World";

    var select1 = document.getElementById("select-1");
    var select2 = document.getElementById("select-2");
    var select3 = document.getElementById("select-3");
    console.log(select1.options[select1.selectedIndex].value);
}
